import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NguoiDungModule } from './nguoi-dung/nguoi-dung.module';
import { ConfigModule } from '@nestjs/config';
import { AuthenModule } from './authen/authen.module';
import { PhongModule } from './phong/phong.module';
import { DatPhongModule } from './dat_phong/dat_phong.module';
import { CommentModule } from './comment/comment.module';
import { LocationModule } from './location/location.module';
@Module({
  imports: [NguoiDungModule,
    ConfigModule.forRoot({
      isGlobal: true
    }),
    AuthenModule,
    PhongModule,
    DatPhongModule,
    CommentModule,
    LocationModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
