import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'
import { errorCode, successCode } from '../config/respone.service'

const prisma = new PrismaClient()

@Injectable()
export class PhongService {
    async getAllPhong() {
        try {
            let data = await prisma.phong.findMany()
            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async createPhong(phongCreate) {
        try {
            await prisma.phong.create({ data: phongCreate })
            return successCode(phongCreate)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async getPhongViTri(ma_vi_tri) {
        try {
            let data = await prisma.phong.findMany({
                where: { ma_vi_tri: ma_vi_tri * 1 }
            })
            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async getSearchPhong(pageIndex, pageSize, keyword) {
        try {
            let data = await prisma.phong.findMany({
                where: { ten_phong: { contains: keyword } },
                skip: (parseInt(pageIndex) - 1) * parseInt(pageSize),
                take: parseInt(pageSize)
            })

            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async getPhongId(id) {
        try {
            let data = await prisma.phong.findFirst({
                where: { id: id * 1 }
            })
            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async updatePhong(id, phongUpdate) {
        try {
            await prisma.phong.update({
                data: phongUpdate,
                where: { id }
            })

            return successCode("Update successful!")
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async deletePhong(id) {
        try {
            await prisma.phong.delete({ where: { id } })

            return successCode("Delete successful!")
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async uploadAvatar(id, fileUpload) {
        try {
            let phongData = await prisma.phong.findFirst({
                where: { id }
            })

            phongData.hinh_anh = fileUpload.filename
            await prisma.phong.update({ data: phongData, where: { id } })

            return successCode("Upload successful!")
        } catch (err) {
            return errorCode(err.message)
        }
    }
}
