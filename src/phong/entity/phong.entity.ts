export class phongCreateType {
    "ten_phong": string;
    "khach": number;
    "phong_ngu": number;
    "giuong": number;
    "phong_tam": number;
    "mo_ta": string;
    "gia_tien": number;
    "may_giat": string;
    "ban_la": string;
    "tivi": string;
    "dieu_hoa": string;
    "wifi": string;
    "bep": string;
    "do_xe": string;
    "hoBoi": string;
    "ban_ui": string;
    "ma_vi_tri": number;
}