import { Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { PhongService } from './phong.service';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@UseGuards(AuthGuard("jwt"))
@Controller('phong')
export class PhongController {
    constructor(private readonly phongService: PhongService) { }

    @Get()
    getAllPhong() {
        return this.phongService.getAllPhong();
    }

    @Post()
    createPhong(@Body() phongCreate) {
        return this.phongService.createPhong(phongCreate);
    }

    @Get('lay-phong-theo-vi-tri/:ma_vi_tri')
    getPhongViTri(@Param('ma_vi_tri') ma_vi_tri) {
        return this.phongService.getPhongViTri(ma_vi_tri);
    }

    @Get('phan-trang-tim-kiem')
    getSearchPhong(@Query('pageIndex') pageIndex, @Query('pageSize') pageSize, @Query('keyword') keyword) {
        return this.phongService.getSearchPhong(pageIndex, pageSize, keyword)
    }

    @Get(':id')
    getPhongId(@Param('id') id) {
        return this.phongService.getPhongId(id)
    }

    @Put(':id')
    updatePhong(@Param('id') id, @Body() phongUpdate) {
        return this.phongService.updatePhong(parseInt(id), phongUpdate)
    }

    @Delete(':id')
    deletaPhong(@Param('id') id) {
        return this.phongService.deletePhong(parseInt(id))
    }

    @UseInterceptors(FileInterceptor("file", {
        storage: diskStorage({
            destination: process.cwd() + "/public/hinh-phong",
            filename: (req, file, callback) => {
                let date = new Date();
                return callback(null, date.getTime() + "_" + file.originalname)
            }
        })
    }))
    @Post('upload/:id')
    uploadAvatar(@Param('id') id: string, @UploadedFile('file') fileUpload: Express.Multer.File) {
        return this.phongService.uploadAvatar(+id, fileUpload)
    }
}
