import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { commentType } from './entities/comment.entity';

const prisma = new PrismaClient();

@Injectable()
export class CommentService {
  async create(comment: commentType) {
    let dataComment = await prisma.binh_luan.create({
      data: comment
    })
    return dataComment;
  }

  async findAll() {
    let data = await prisma.binh_luan.findMany()
    return data;
  }

  async findOne(roomId: number) {
    let data = await prisma.binh_luan.findFirst({
      where: { ma_phong: Number(roomId) }
    })
    return data;
  }

  async update(commentId: number, comment: commentType) {
    let dataComment = await prisma.binh_luan.update({
      where: { id: Number(commentId) },
      data: comment
    })
    return dataComment;
  }

  async remove(commentId: number) {
    let dataComment = await prisma.binh_luan.delete({
      where: {id: Number(commentId)}
    })
    return dataComment;
  }
}
