import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { commentType } from './entities/comment.entity';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard("jwt"))
@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post('create-comment')
  create(@Body() comment: commentType) {
    return this.commentService.create(comment);
  }

  @Get('get-comment')
  findAll() {
    return this.commentService.findAll();
  }

  @Get('get-comment-by-room-id/:roomId')
  findOne(@Param('roomId') roomId: number) {
    return this.commentService.findOne(+roomId);
  }

  @Patch('update-comment/:commentId')
  update(@Param('commentId') commentId: number, @Body() comment: commentType) {
    return this.commentService.update(commentId, comment);
  }

  @Delete('delete-comment/:commentId')
  remove(@Param('commentId') commentId: number) {
    return this.commentService.remove(commentId);
  }
}
