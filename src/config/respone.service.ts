const successCode = (data) => {
    return {
        statusCode: 200,
        content: {
            data
        }
    }
}

const failCode = (message) => {
    return {
        statusCode: 400,
        content: message
    }
}

const errorCode = (message) => {
    return {
        statusCode: 500,
        content: message
    }
}

export { successCode, failCode, errorCode }
