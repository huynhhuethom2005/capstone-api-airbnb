import { Module } from '@nestjs/common';
import { DatPhongController } from './dat_phong.controller';
import { DatPhongService } from './dat_phong.service';

@Module({
  controllers: [DatPhongController],
  providers: [DatPhongService]
})
export class DatPhongModule {}
