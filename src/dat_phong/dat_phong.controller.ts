import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { DatPhongService } from './dat_phong.service';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard("jwt"))
@Controller('dat-phong')
export class DatPhongController {
    constructor(private readonly datPhongService: DatPhongService) { }

    @Get()
    getAllDatPhong() {
        return this.datPhongService.getAllDatPhong()
    }

    @Post()
    createDatPhong(@Body() dataDatPhong) {
        return this.datPhongService.createDatPhong(dataDatPhong)
    }

    @Get(':id')
    getPhongId(@Param('id') id) {
        return this.datPhongService.getPhongId(+id)
    }

    @Put(':id')
    updateDatPhong(@Body() dataDatPhong, @Param('id') id) {
        return this.datPhongService.updateDatPhong(+id, dataDatPhong)
    }

    @Delete(':id')
    deleteDatPhong(@Param('id') id) {
        return this.datPhongService.deleteDatPhong(+id)
    }

    @Get('lay-theo-nguoi-dung/:ma_nguoi_dat')
    getDatPhongNguoiDung(@Param('ma_nguoi_dat') ma_nguoi_dat) {
        return this.datPhongService.getDatPhongNguoiDung(+ma_nguoi_dat)
    }
}
