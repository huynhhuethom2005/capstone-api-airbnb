import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'
import { errorCode, successCode } from 'src/config/respone.service';

const prisma = new PrismaClient()

@Injectable()
export class DatPhongService {
    async getAllDatPhong() {
        try {
            let data = await prisma.dat_phong.findMany()
            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async createDatPhong(dataDatPhong) {
        try {
            await prisma.dat_phong.create({ data: dataDatPhong })
            return successCode(dataDatPhong)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async getPhongId(id) {
        try {
            let data = await prisma.dat_phong.findFirst({
                where: { id },
            })

            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async updateDatPhong(id, dataDatPhong) {
        try {
            await prisma.dat_phong.update({
                data: dataDatPhong,
                where: { id },
            })

            return successCode('Updated data successfully')
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async deleteDatPhong(id) {
        try {
            await prisma.dat_phong.delete({ where: { id } })
            return successCode('Deleted data successfully')
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async getDatPhongNguoiDung(ma_nguoi_dat) {
        try {
            let data = await prisma.dat_phong.findMany({
                where: { ma_nguoi_dat }
            })

            return successCode(data)
        } catch (err) {
            return errorCode(err.message)
        }
    }
}

