import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { LocationService } from './location.service';
import { locationType } from './entities/location.entity';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard("jwt"))
@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) { }

  @Post('create-location')
  create(@Body() locationData: locationType) {
    return this.locationService.create(locationData);
  }

  @Get('get-location')
  findAll() {
    return this.locationService.findAll();
  }

  @Get('get-location/:localtionId')
  findOne(@Param('localtionId') localtionId: number) {
    return this.locationService.findOne(localtionId);
  }

  @Patch('update-location/:localtionId')
  update(@Param('localtionId') localtionId: number, @Body() locationData: locationType) {
    return this.locationService.update(localtionId, locationData);
  }

  @Post('location-pagination/:page/:pageSize')
    paginationUser(@Param('page') page: number, @Param('pageSize') pageSize: number) {
        return this.locationService.paginationUser(page, pageSize);
    }

  @Delete('delete-location/:localtionId')
  remove(@Param('localtionId') localtionId: number) {
    return this.locationService.remove(localtionId);
  }
}
