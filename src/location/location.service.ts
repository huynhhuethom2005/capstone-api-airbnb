import { Injectable } from '@nestjs/common';
import { locationType } from './entities/location.entity';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Injectable()
export class LocationService {
  async create(locationData: locationType) {
    let dataLocation = await prisma.vi_tri.create({
      data: locationData
    })
    return dataLocation;
  }

  async findAll() {
    let data = await prisma.vi_tri.findMany()

    return data;
  }

  async findOne(localtionId: number) {
    let data = await prisma.vi_tri.findFirst({
      where: { id: Number(localtionId) }
    })

    return data;
  }

  async update(localtionId: number, locationData: locationType) {
    let dataLocaltion = await prisma.vi_tri.update({
      where: { id: Number(localtionId) },
      data: locationData
    })

    return dataLocaltion;
  }

  async remove(localtionId: number) {
    let dataDelete = await prisma.vi_tri.delete({
      where: { id: Number(localtionId) }
    })
    return dataDelete;
  }

  async paginationUser(page: number, pageSize: number) {
    let index = (page - 1) * pageSize;

    let data = await prisma.vi_tri.findMany({
      skip: index,
      take: Number(pageSize)
    })

    return data;
  }
}
