import { Body, Controller, Get, Post, Param, Patch, Delete, UseGuards } from '@nestjs/common';
import { NguoiDungService } from './nguoi-dung.service';
import { userType } from './entity/nguoi-dung.entity';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard("jwt"))
@Controller('user')
export class NguoiDungController {
    constructor(private readonly nguoiDungService: NguoiDungService) { }

    @Get('get-user')
    findAllNguoiDung() {
        return this.nguoiDungService.findAllUser();
    }

    @Get('get-user/:userId')
    findNguoiDungFollowId(@Param('userId') userId: number) {
        return this.nguoiDungService.findUserFollowId(userId);
    }

    @Get('search/:userName')
    searchNguoiDungFollowName(@Param('userName') userName: string) {
        return this.nguoiDungService.searchUserFollowName(userName);
    }

    @Post('create-user')
    createUser(@Body() user: userType) {
        return this.nguoiDungService.createUser(user);
    }

    @Post('user-pagination/:page/:pageSize')
    paginationUser(@Param('page') page: number, @Param('pageSize') pageSize: number) {
        return this.nguoiDungService.paginationUser(page, pageSize);
    }

    @Patch('update-user/:userId')
    updateUser(@Param('userId') userId: number, @Body() user: any) {
        return this.nguoiDungService.updateUser(userId, user)
    }

    @Delete('delete-user/:userId')
    deleteUser(@Param('userId') userId: number) {
        return this.nguoiDungService.deleteUser(userId)
    }
}
