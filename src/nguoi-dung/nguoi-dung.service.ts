import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { userType } from './entity/nguoi-dung.entity';
import { async } from 'rxjs';

const prisma = new PrismaClient();

@Injectable()
export class NguoiDungService {

    async findAllUser() {
        let data = await prisma.nguoi_dung.findMany();

        return data
    }

    async findUserFollowId(userId: number) {
        let data = await prisma.nguoi_dung.findMany({
            where: {
                id: Number(userId)
            }
        });

        return data
    }

    async searchUserFollowName(userName: string) {
        let data = await prisma.nguoi_dung.findMany({
            where: {
                name: userName
            }
        });

        return data
    }

    async createUser(user: userType) {
        let dataUser = await prisma.nguoi_dung.create({ data: user });
        return dataUser;
    }

    async paginationUser(page: number, pageSize: number) {

        let index = (page - 1) * pageSize;

        let data = await prisma.nguoi_dung.findMany({
            skip: index,
            take: Number(pageSize)
        })

        return data;
    }

    async updateUser(userId: number, userData: any) {
        let dataUser = await prisma.nguoi_dung.update({
            where: {id: Number(userId)},
            data: userData
        })
        return dataUser;
    }

    async deleteUser(userId: number) {
        let dataUser = await prisma.nguoi_dung.delete({
            where: {id: Number(userId)}
        })
        return dataUser
    }
}
