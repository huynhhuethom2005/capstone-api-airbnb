import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'
import { userInfoType, userLoginType } from './entity/authen.entity';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { errorCode, failCode, successCode } from 'src/config/respone.service';

const prisma = new PrismaClient()

@Injectable()
export class AuthenService {
    constructor(private jwtService: JwtService, private config: ConfigService) { }

    async userSignup(userInfo: userInfoType) {
        try {
            let checkEmail = await prisma.nguoi_dung.findFirst({
                where: { email: userInfo.email }
            })
            if (checkEmail) {
                return failCode("Email already registered!")
            } else {
                let data = { ...userInfo, pass_word: bcrypt.hashSync(userInfo.pass_word, 10) }
                await prisma.nguoi_dung.create({ data })
                return successCode("Signup successful!")
            }
        } catch (err) {
            return errorCode(err.message)
        }
    }

    async userSignin(userLogin: userLoginType) {
        try {
            let checkEmail = await prisma.nguoi_dung.findFirst({
                where: { email: userLogin.email }
            })

            if (checkEmail) {
                if (bcrypt.compareSync(userLogin.pass_word, checkEmail.pass_word)) {
                    let { pass_word, ...data } = { ...userLogin }
                    let token = this.jwtService.sign(
                        { data: data },
                        {
                            expiresIn: '1h',
                            secret: this.config.get('SECRET_KEY')
                        }
                    )

                    return successCode(token)
                } else {
                    return failCode("Passwords incorrect")
                }
            } else {
                return failCode("Email incorrect!")
            }
        } catch (err) {
            return errorCode(err.message)
        }
    }
}
