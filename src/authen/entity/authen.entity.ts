export class userLoginType {
    email: string;
    pass_word: string;
}

export class userInfoType {
    name: string;
    email: string;
    pass_word: string;
    phone: string;
    birth_day: string;
    genner: string;
    role: string;
}