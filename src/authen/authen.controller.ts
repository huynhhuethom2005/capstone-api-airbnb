import { Body, Controller, Post } from '@nestjs/common';
import { AuthenService } from './authen.service';
import { userInfoType, userLoginType } from './entity/authen.entity';

@Controller('authen')
export class AuthenController {
    constructor(private readonly authenService: AuthenService) { }

    @Post('signup')
    userSignup(@Body() userInfo: userInfoType) {
        return this.authenService.userSignup(userInfo)
    }

    @Post('signin')
    userSignin(@Body() userLogin: userLoginType) {
        return this.authenService.userSignin(userLogin)
    }
}
